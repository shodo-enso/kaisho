# kaisho

Wherein I produce something which runs.

## Running kaisho

The function is designed to run anywhere javascript can be run. To execute the function directly, a helper script `cmd.js` has been created to invoke the function with input from an environment variable.

The helper script is designed to be executed by "deno", a modern alternative to node.js. As such, a prerequisite is to install deno as described here: https://deno.land/#installation

To evaluate the result of the input `10`, you would pass 10 as the value of the environment variable `ATLE` (**A**lle **T**all **L**avere **E**nn) as illustrated below:

```bash
ATLE=10 deno run --allow-env cmd.ts
```
Sample execution: 
```bash
$ ATLE=4510 deno run --allow-env cmd.ts
Compile file:///....cmd.ts
>>  Forsøker å finne summen av multipler av 3 eller 5 av alle hele tall "0..4510"

    Dersom det oppgis et tall som ikke kan identifiseres som et helt tall, vil
    resultatet være -1. Dersom tallet eller resultatet overskrider maksimalt trygg størrelse
    vil tallet -2 være returnert. Alle resultat som er større enn 0 er det korrekte svaret.
4745273
```

### Permitted outputs:

kaisho will output a positive integer if it was able to correctly calculate the result.

| Output               | Meaning                                                            |
|----------------------|--------------------------------------------------------------------|
| Any Positive Integer | The correct result as calculated                                   |
| -1                   | Invalid type supplied                                              |
| -2                   | Input or calculated result exceeds maximum safe range for integers |

## Notes and limitations

- *Floating point inputs are valid*  
According to specification, there is no limit on the input, only that it shall operate on natural numbers smaller than the input. As such, the input must be numeric in some sense.   
The function will therefore treat the value `10.0001` as larger than `10` and thus consider `10` a valid member of the set below `10.0001`. An input of `10` would exclude the value `10` from the running sum as it is equal to the input (and therefore not "less").

- *Range of inputs/outputs*   
Both deno and node are lacking tail-call optimization (or PTC), as is every browser other than Safari apparently. This does severely limit the range where this function can be useful, for obvious reasons. On my machine, node limits this maximum input value to `8942`, and deno at `8787`. Utterly useless. Why didn't i just write this in `caml light` as I thought from the start. But noooo, I had to play with deno. 



## General plan

In order to achieve my goals, without overkilling too much, I'll keep my train of thought straight.

My chosen approach here will be simple.  I will roughly sketch the function that I wish to demonstrate, before moving on to defining and writing tests for said function. I will then implement the function properly and iteratively against the defined tests.  Finally, I will incorporate a simple ci/cd pipeline which will execute the tests against the function.

In all honesty, what I wish to achieve is wrapping the function in a simple REST API exposed by deno delivered in a container running on kubernetes.  Ideally as well, this will allow the building of a simple web interface to dynamically execute the function against arbitrary inputs delivering results safely and efficiently. 

This will also require a simple terraform template to stand up a k8s cluster on digitalocean, as well as bindings to a load balancer and a domain, ingress controller with automatic certificate management from lets-encrypt (not going to mess around with istio or kuma at this point), as well as building a simple static page to host a vue.js input form towards the api.

I am going to promise myself that I do not do that, even though that would be a lot of fun. 

## Notes

Hit on a pretty fun bug (likely https://github.com/denoland/deno/pull/6000 and/or https://github.com/denoland/deno/issues/5982), whose obvious ramifications for this project is to just implement it in typescript. Which is damned easy. But hey, I enjoy causing myself pain for some reason :D