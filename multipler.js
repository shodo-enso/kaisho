/***** Programmet skal a formen:
 * Int FinnMultipler(int AlleTallLavereEnn) {}
 * og skal returnere summen av tallene 0..AlleTallLavereEnn
 * som er multipler av 3 og/eller 5.
 * 
 * Programmet implementeres enkelt på funksjonelt vis.
 * Det vil i dette tilfellet benyttes tail-recursion.
 * 
 */

// findMultiples takes two arguments :
// - the initial input "N"
// - an accumulator that will pass the running tally (initialized at zero)
export const findMultiples = (N, accumulator = 0) => {
  // If given a float input (valid according to spec), ensure the natural part
  // of the float is considered as it is less than the number specified.
  // 10 < 10.00001, therefore 10 is valid for accumulator.
  if (Number.parseInt(N) < N) {
    N = Number.parseInt(N) + 1;
  }
  // Simplest form of safety, return -1 if N !== Int.
  if (!Number.isInteger(N) || isNaN(N)) {
    return -1;
  }

  // Stack overflow protectionism, returning -2 for integers outside of safe operating parameters
  if (N > Number.MAX_SAFE_INTEGER || accumulator > Number.MAX_SAFE_INTEGER) {
    return -2;
  }

  // Return the accumulator once worthwhile recursion has been reached
  // bonus: protects against negative values for N, returning default = 0
  if (N <= 3) {
    return accumulator;
  }

  /***
     * If any of above conditions not met:
     * Recurse with next lowest integer value for N
     * Increase accumulator if such value is a multiple of 3 or 5.
     */
  return findMultiples(
    N - 1,
    ((N - 1) % 3 === 0 || (N - 1) % 5 === 0)
      ? accumulator + N - 1
      : accumulator,
  );
};
