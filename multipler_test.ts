/****
 * Wherein we define tests for the findMultiples program.
 * Will write tests presuming the Deno environment, because..
 * I haven't tried that before. It'll be interesting.
 */

/****
  * Test Strategy:
  * - Initial test for happy cases with known results.
  * I.e. input of 10 shall return 23.
  * 
  * - Testing type-handling, no longer tolerating unsafe typing (returns -1)
  *
  * - Testing non-integer types and typical failure modes
  * 
  * - Edge case testing: 
  * inputs with 0 as expected output (0,1,2)
  * negative inputs, etc.
  * inputs creating outputs larger than max INT
  * (side benefit being monitoring execution behaviour)
  * 
  * - Creative testing, possibly leveraging 
  * https://github.com/minimaxir/big-list-of-naughty-strings
  * 
  */

import { findMultiples } from "./multipler.js";
import { assertEquals } from "https://deno.land/std@0.55.0/testing/asserts.ts";

Deno.test("Clear Success Cases", () => {
  assertEquals(findMultiples(3), 0);
  assertEquals(findMultiples(4), 3);
  assertEquals(findMultiples(10), 23);
  assertEquals(findMultiples(11), 33);
  assertEquals(findMultiples(15), 45);
});

Deno.test("String inputs", () => {
  assertEquals(findMultiples("10"), -1);
  assertEquals(findMultiples("11"), -1);
  assertEquals(findMultiples("15"), -1);
});

Deno.test("Float inputs", () => {
  assertEquals(findMultiples(10.0), 23);
  assertEquals(findMultiples(13.0), 45);
});

Deno.test("Float inputs with non-zero mantissas", () => {
  assertEquals(findMultiples(10.0234), 33);
  assertEquals(findMultiples(13.8721), 45);
  assertEquals(findMultiples(14 / 3), 3);
});

// NaN or type-invalid inputs return -1
Deno.test("Invalid input -> Graceful failure", () => {
  assertEquals(findMultiples("potato"), -1);
  assertEquals(findMultiples([15]), -1);
  assertEquals(findMultiples({ value: 15 }), -1);
});

Deno.test("Negative values produce zero as output", () => {
  assertEquals(findMultiples(-1), 0);
  assertEquals(findMultiples(-1 * Number.MAX_SAFE_INTEGER), 0);
});

Deno.test("Positive values smaller than smallest divisor produces zero as output", () => {
  assertEquals(findMultiples(0), 0);
  assertEquals(findMultiples(1), 0);
  assertEquals(findMultiples(2), 0);
});

Deno.test("Testing handling of max safe integer input", () => {
  // due to algorithms nature, this is required to produce a result greater than MAX_SAFE_INTEGER.
  // However, due to how javascript works, let's start laughing.

  // Hardwired to return -2 in the event of overflow detection.
  assertEquals(findMultiples(Number.MAX_SAFE_INTEGER), -2);
  // Running without protection kills the stack at accumulator value 39217345555122270000
});
