import { findMultiples } from "./multipler.js";

const AlleTallLavereEnn = Deno.env.get("ATLE");

console.log(
  `>>  Forsøker å finne summen av multipler av 3 eller 5 av alle hele tall "0..` +AlleTallLavereEnn + `"
    
    Dersom det oppgis et tall som ikke kan identifiseres som et helt tall, vil 
    resultatet være -1. Dersom tallet eller resultatet overskrider maksimalt trygg størrelse 
    vil tallet -2 være returnert. Alle resultat som er større enn 0 er det korrekte svaret.`,
);
if (AlleTallLavereEnn !== undefined) {
  let result = findMultiples(Number.parseFloat(AlleTallLavereEnn));
  console.log(result);
  // Terminate with positive value of negative result if error condition reached.
  result >= 0 ? Deno.exit(0) : Deno.exit(result * -1);
} else {
  console.log(
    `\n!>> Dessverre er det ikke definert noe innhold i variablen 'ATLE', 
    og dermed ingen respons. Forsøk å eksekver med prefixen 'ATLE={tall}' i fremtiden.`,
  );
  Deno.exit(3);
}
